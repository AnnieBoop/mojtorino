'use strict';

angular.module('bankApp.version', [
  'bankApp.version.interpolate-filter',
  'bankApp.version.version-directive'
])

.value('version', '0.1');
