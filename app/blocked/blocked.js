'use strict';

angular.module('myApp.blocked', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/blocked', {
    templateUrl: 'blocked/blocked.html',
    controller: 'BlockedCtrl'
  });
}])

.controller('BlockedCtrl', [function() {

}]);