/// <reference path="../../app.js" />

bankApp.factory('consultazionePoteriService', ['$http', '$q', function($http, $q) {
    
    return {
        getConsultazionePoteri: function() {

            var deffered = $q.defer();

            $http({
                method: 'GET',
                url: 'http://localhost:3000/consultazionePoteri'
              }).then(function successCallback(data, status, header, config) {
                console.log(data);
                
                  deffered.resolve(data);
                
                }, function errorCallback(data, status, header, config) {

                    deffered.reject(status);   

                });

                return deffered.promise;
            
        }   
    };
  }]
);