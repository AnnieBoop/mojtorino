/// <reference path="../../app.js" />

bankApp.factory('listaAccordiService', ['$http', '$q', function($http, $q) {
    
    return {
        getListaAccordi: function() {

            var deffered = $q.defer();

            $http({
                method: 'GET',
                url: 'http://localhost:3000/listaAccordi'
              }).then(function successCallback(data, status, header, config) {
                console.log(data);
                
                  deffered.resolve(data);
                
                }, function errorCallback(data, status, header, config) {

                    deffered.reject(status);   

                });

                return deffered.promise;
            
        }   
    };
  }]
);