/// <reference path="../../app.js" />

bankApp.factory('ricercaValoriService', ['$http', '$q', function($http, $q) {
    
    return {
        getRicercaValori: function() {

            var deffered = $q.defer();

            $http({
                method: 'GET',
                url: 'http://localhost:3000/ricercaValori'
              }).then(function successCallback(data, status, header, config) {
                console.log(data);
                
                  deffered.resolve(data);
                
                }, function errorCallback(data, status, header, config) {

                    deffered.reject(status);   

                });

                return deffered.promise;
            
        }   
    };
  }]
);