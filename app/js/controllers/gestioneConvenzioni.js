'use strict';

angular.module('bankApp.gestioneConvenzioni', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/gestioneConvenzioni', {
    templateUrl: 'js/views/gestioneConvenzioni.html',
    controller: 'gestioneConvenzioniCtrl'
  });
}])

.controller('gestioneConvenzioniCtrl', ['$scope', '$http', '$location', '$routeParams','gestioneConvenzioniService', function($scope, $http, $location, $routeParams, gestioneConvenzioniService) {

  console.log(gestioneConvenzioniService.getGestioneConvenzioni());

  $scope.gestioneConvenzioni = gestioneConvenzioniService.getGestioneConvenzioni();

  $scope.gestioneConvenzioni.then(function(gestioneConvenzioni) {
    $scope.gestioneConvenzioniAll = gestioneConvenzioni.data;
  }, function(status) {

  });
}]);