'use strict';

angular.module('bankApp.consultazionePoteri', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/consultazionePoteri', {
    templateUrl: 'js/views/consultazionePoteri.html',
    controller: 'consultazionePoteriCtrl'
  });
}])

.controller('consultazionePoteriCtrl', ['$scope', '$http', '$location', '$routeParams','consultazionePoteriService', function($scope, $http, $location, $routeParams, consultazionePoteriService) {

  console.log(consultazionePoteriService.getConsultazionePoteri());

  $scope.consultazionePoteri = consultazionePoteriService.getConsultazionePoteri();

  $scope.consultazionePoteri.then(function(consultazionePoteri) {
    $scope.consultazionePoteriAll = consultazionePoteri.data;
  }, function(status) {

  });
}]);
