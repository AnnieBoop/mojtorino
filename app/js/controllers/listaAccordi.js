'use strict';

angular.module('bankApp.listaAccordi', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/listaAccordi', {
    templateUrl: 'js/views/listaAccordi.html',
    controller: 'listaAccordiCtrl'
  });
}])

.controller('listaAccordiCtrl', ['$scope', '$http', '$location', '$routeParams','listaAccordiService', function($scope, $http, $location, $routeParams, listaAccordiService) {

  console.log(listaAccordiService.getListaAccordi());

  $scope.listaAccordi = listaAccordiService.getListaAccordi();

  $scope.listaAccordi.then(function(listaAccordi) {
    $scope.listaAccordiAll = listaAccordi.data;
  }, function(status) {

  });
}]);
