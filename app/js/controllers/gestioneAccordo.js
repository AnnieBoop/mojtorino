'use strict';

angular.module('bankApp.gestioneAccordo', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/gestioneAccordo', {
    templateUrl: 'js/views/gestioneAccordo.html',
    controller: 'gestioneAccordoCtrl'
  });
}])

.controller('gestioneAccordoCtrl', ['$scope', '$http', '$location', '$routeParams','gestioneAccordoService', function($scope, $http, $location, $routeParams, gestioneAccordoService) {

  console.log(gestioneAccordoService.getGestioneAccordo());

  $scope.gestioneAccordo = gestioneAccordoService.getGestioneAccordo();

  $scope.gestioneAccordo.then(function(gestioneAccordo) {
    $scope.gestioneAccordoAll = gestioneAccordo.data;
  }, function(status) {

  });
}]);
