'use strict';

angular.module('bankApp.ricercaValori', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/ricercaValori', {
    templateUrl: 'js/views/ricercaValori.html',
    controller: 'ricercaValoriCtrl'
  });
}])

.controller('ricercaValoriCtrl', ['$scope', '$http', '$location', '$routeParams','ricercaValoriService', function($scope, $http, $location, $routeParams, ricercaValoriService) {

  console.log(ricercaValoriService.getRicercaValori());

  $scope.ricercaValori = ricercaValoriService.getRicercaValori();

  $scope.ricercaValori.then(function(ricercaValori) {
    $scope.ricercaValoriAll = ricercaValori.data;
  }, function(status) {

  });
}]);