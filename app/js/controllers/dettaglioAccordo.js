'use strict';

angular.module('bankApp.dettaglioAccordo', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/dettaglioAccordo', {
    templateUrl: 'js/views/dettaglioAccordo.html',
    controller: 'dettaglioAccordoCtrl'
  });
}])

.controller('dettaglioAccordoCtrl', ['$scope', '$http', '$location', '$routeParams','accordoService', function($scope, $http, $location, $routeParams, accordoService) {

  console.log(accordoService.getDettaglioAccordo());

  $scope.dettaglioAccordo = accordoService.getDettaglioAccordo();

  $scope.dettaglioAccordo.then(function(dettaglioAccordo) {
    $scope.dettaglioAccordoAll = dettaglioAccordo.data;
  }, function(status) {

  });
}]);
