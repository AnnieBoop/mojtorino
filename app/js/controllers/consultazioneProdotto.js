'use strict';

angular.module('bankApp.consultazioneProdotto', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/consultazioneProdotto', {
    templateUrl: 'js/views/consultazioneProdotto.html',
    controller: 'consultazioneProdottoCtrl'
  });
}])

.controller('consultazioneProdottoCtrl', ['$scope', '$http', '$location', '$routeParams','consultazioneProdottoService', function($scope, $http, $location, $routeParams, consultazioneProdottoService) {

  console.log(consultazioneProdottoService.getConsultazioneProdotto());

  $scope.consultazioneProdotto = consultazioneProdottoService.getConsultazioneProdotto();

  $scope.consultazioneProdotto.then(function(consultazioneProdotto) {
    $scope.consultazioneProdottoAll = consultazioneProdotto.data;
  }, function(status) {

  });
}]);
