'use strict';

angular.module('bankApp.caratteristicheOperative', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/caratteristicheOperative', {
    templateUrl: 'js/views/caratteristicheOperative.html',
    controller: 'caratteristicheOperativeCtrl'
  });
}])

.controller('caratteristicheOperativeCtrl', ['$scope', '$http', '$location', '$routeParams','caratteristicheOperativeService', function($scope, $http, $location, $routeParams, caratteristicheOperativeService) {

  console.log(caratteristicheOperativeService.getCaratteristicheOperative());

  $scope.caratteristicheOperative = caratteristicheOperativeService.getCaratteristicheOperative();

  $scope.caratteristicheOperative.then(function(caratteristicheOperative) {
    $scope.caratteristicheOperativeAll = caratteristicheOperative.data;
  }, function(status) {

  });
}]);
